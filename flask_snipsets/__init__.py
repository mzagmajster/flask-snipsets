from jinja2 import Markup
from flask.json import JSONEncoder

from flask_login import current_user


class CustomJSONEncoder(JSONEncoder):
	"""This class adds support for lazy translation texts to Flask's
	JSON encoder. This is necessary when flashing translated texts."""
	def default(self, obj):
		from speaklater import is_lazy_string
		if is_lazy_string(obj):
			return unicode(obj)  # python 2
		return super(CustomJSONEncoder, self).default(obj)


class MomentJS(object):
	"""Enables access to MomentJS functionality inside jinja2 templates."""
	def __init__(self, timestamp):
		self.timestamp = timestamp

	def render(self, format):
		try:
			language_code = current_user.lang.code
		except:
			# Default language.
			language_code = 'sl'
		try:
			return Markup(
				"<script>moment.locale(\"%s\");\ndocument.write(moment(\"%s\").%s);\n</script>" %
				(language_code, self.timestamp.strftime("%Y-%m-%dT%H:%M:%S Z"), format)
			)
		except:
			return ''

	def format(self, fmt):
		return self.render("format(\"%s\")" % fmt)

	def calendar(self):
		return self.render("calendar()")

	def fromNow(self):
		return self.render("fromNow()")
