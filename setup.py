from distutils.core import setup

setup(
	name='Flask-snipsets',
	version='0.1',
	packages=['flask_snipsets'],
	url='http://maticzagmajster.ddns.net/',
	license='',
	author='Matic Zagmajster',
	author_email='zagm101@gmail.com',
	description='Useful code for Flask projects.'
)
